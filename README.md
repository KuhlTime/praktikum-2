### Praktikum 2 - Software Engineering 2

- Name: **Andr� Kuhlmann**
- Matr.-Nr:	**779690**
- Erstellt: **24. April 2020**
- Java: **14**
- IDE: **[IntelliJ IDEA](https://www.jetbrains.com/de-de/idea/)**

#### Aufgaben:  
1.  Kopieren Sie das Projekt mit GUI  
2.  Erstellen Sie Klassen f�r die Informationsobjekte und den Zugriff auf die Informationsobjekte gem�� der [Anleitung](http://openbook.rheinwerk-verlag.de/java7/1507_13_003.html#dodtp60d8d19d-a252-446c-a1a3-4e39ebf1f06f)