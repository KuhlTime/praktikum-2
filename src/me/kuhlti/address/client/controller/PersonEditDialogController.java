package me.kuhlti.address.client.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import me.kuhlti.address.client.model.Person;
import me.kuhlti.address.client.util.DateUtil;

public class PersonEditDialogController {

    // ====== Properties ======

    private Stage dialogStage;
    private Person person;
    private boolean okClicked = false;


    // ====== Computed Props ======

    public boolean isOkClicked() {
        return okClicked;
    }


    // ====== Outlets ======

    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML private TextField streetField;
    @FXML private TextField postalCodeField;
    @FXML private TextField cityField;
    @FXML private TextField birthdayField;


    // ====== Callbacks ======

    @FXML private void initialize() {}


    // ====== Actions ======

    @FXML private void handleOk() {
        if (isInputValid()) {
            person.setFirstName(firstNameField.getText());
            person.setLastName(lastNameField.getText());
            person.setStreet(streetField.getText());
            person.setPostalCode(Integer.parseInt(postalCodeField.getText()));
            person.setCity(cityField.getText());
            person.setBirthday(DateUtil.parse(birthdayField.getText()));

            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML private void handleCancel() {
        dialogStage.close();
    }


    // ====== Functions ======

    public void setDialogStage(Stage stage) {
        this.dialogStage = stage;
    }

    public void setPerson(Person person) {
        this.person = person;

        firstNameField.setText(person.getFirstName());
        lastNameField.setText(person.getLastName());
        streetField.setText(person.getStreet());
        postalCodeField.setText(Integer.toString(person.getPostalCode()));
        cityField.setText(person.getCity());
        birthdayField.setText(DateUtil.format(person.getBirthday()));
        birthdayField.setPromptText("dd.mm.yyyy");
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (isInvalid(firstNameField)) {
            errorMessage += "• Vorname\n";
        }
        if (isInvalid(lastNameField)) {
            errorMessage += "• Nachname\n";
        }
        if (isInvalid(streetField)) {
            errorMessage += "• Straße\n";
        }
        if (isInvalid(cityField)) {
            errorMessage += "• Stadt\n";
        }

        if (isInvalid(postalCodeField)) {
            errorMessage += "• Postleitzahl\n";
        } else {
            try {
                Integer.parseInt(postalCodeField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Die Postleitzahl konnte nicht gelesen werden\n";
            }
        }

        if (isInvalid(birthdayField)) {
            errorMessage += "• Geburtsdatum\n";
        } else {
            if (!DateUtil.validDate(birthdayField.getText())) {
                errorMessage += String.format("Der Geburtstag muss folgendes format haben: %s!\n", DateUtil.DATE_PATTERN);
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show error Message
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Fehlerhafte Eingaben");
            alert.setHeaderText("Bitte korrigiere die fehlerhaften Felder");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    private boolean isInvalid(TextField field) {
        return field.getText() == null || field.getText().length() == 0;
    }
}
