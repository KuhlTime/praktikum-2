package me.kuhlti.address.client.controller;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import me.kuhlti.address.client.Main;
import me.kuhlti.address.client.model.PersonManager;
import me.kuhlti.address.client.util.DateUtil;
import me.kuhlti.address.client.model.Person;

import javafx.fxml.FXML;

public class PersonOverviewController {

    // ====== Outlets ======

    @FXML private TableView<Person> personTableView;
    @FXML private TableColumn<Person, String> firstNameColumn;
    @FXML private TableColumn<Person, String> lastNameColumn;
    @FXML private ButtonBar buttonBar;
    @FXML private Button addButton;
    @FXML private Button syncButton;
    @FXML private Button deleteButton;
    @FXML private Button editButton;

    @FXML private GridPane contentGrid;
    @FXML private Label headerLabel;
    @FXML private Label firstNameLabel;
    @FXML private Label lastNameLabel;
    @FXML private Label streetLabel;
    @FXML private Label cityLabel;
    @FXML private Label postalCodeLabel;
    @FXML private Label birthdayLabel;
    @FXML private Label idLabel;

    private Main main;


    // ====== Initializer ======

    public PersonOverviewController() {}


    // ====== Callbacks ======

    @FXML private void initialize() {
        // Initialize the personTableView
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());

        // Clear person details
        showPersonDetails(null);

        // Add an event listener to the selected item property of the table view.
        personTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
    }

    // ====== Functions ======

    public void setMain(Main main) {
        this.main = main;

        // Set items for table view
        personTableView.setItems(PersonManager.getAllPersons());
    }

    private void showPersonDetails(Person person) {
        boolean personAvailable = (person != null);

        headerLabel.setText(personAvailable ? "Detailansicht" : "Wählen Sie eine Person aus!");
        editButton.setDisable(!personAvailable);
        deleteButton.setDisable(!personAvailable);
        contentGrid.setVisible(personAvailable);

        if (!personAvailable) { return; }

        idLabel.setText(person.getID());
        firstNameLabel.setText(person.getFirstName());
        lastNameLabel.setText(person.getLastName());
        cityLabel.setText(person.getCity());
        streetLabel.setText(person.getStreet());
        postalCodeLabel.setText(Integer.toString(person.getPostalCode()));
        birthdayLabel.setText(DateUtil.format(person.getBirthday()));

        syncButton.setTooltip(new Tooltip("Lädt die neusten Daten vom Server herunter"));
    }


    // ====== Actions ======

    @FXML private void handleNewPerson() {
        Person newPerson = Person.emptyPerson();
        boolean okClicked = main.showPersonEditDialog(newPerson);

        if (okClicked) {
            PersonManager.add(newPerson);
        }
    }

    @FXML private void handleSync() {
        PersonManager.sync();
    }

    @FXML private void handleEditPerson() {

        // The edit button is only enabled when a user is selected
        // so there is no need to check if selectedPerson == null

        Person selectedPerson = personTableView.getSelectionModel().getSelectedItem();

        boolean okClicked = main.showPersonEditDialog(selectedPerson);

        if (okClicked) {
            showPersonDetails(selectedPerson);
            PersonManager.update(selectedPerson);
        }
    }

    @FXML private void handleDeletePerson() {

        // Because the delete button is only enabled when a person is selected there is no
        // need to handle the case of no user being selected at this point

        Person selectedPerson = personTableView.getSelectionModel().getSelectedItem();
        PersonManager.delete(selectedPerson);
    }

}
