package me.kuhlti.address.client.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import me.kuhlti.address.client.generated.*;

public class PersonManager {

    // ====== Properties ======

    private static SPersonAdminWS service = new ServerService().getSPersonAdminWSPort();
    private static ObservableList<Person> persons = FXCollections.observableArrayList();


    // ====== Server Calls ======

    public static void add(Person person) {
        service.add(person.toSPerson());
        addLocally(person);
    }

    public static void delete(Person person) {
        service.delete(person.toSPerson());
        deleteLocally(person);
    }

    public static void update(Person person) {
        SPerson sPerson = person.toSPerson();
        service.update(sPerson);
        updateLocally(person);
    }

    public static ObservableList<Person> getAllPersons() {
        return persons;
    }

    /**
     * Syncs the results with the server.
     */
    public static void sync() {
        persons.clear();

        service.getAll().getItem().forEach(sPerson -> {
            // FIXME: Resource intensive, reloads the TableView for every Person added
            persons.add(new Person(sPerson));
        });
    }


    // ====== Local Array Manipulation ======

    private static void addLocally(Person person) {
        // TODO: Check if ID exists
        persons.add(person);
    }

    private static void deleteLocally(Person person) {
        persons.remove(person);
    }

    private static void updateLocally(Person newPerson) {
        for (int i = 0; i < persons.size(); i++) {
            Person person = persons.get(i);

            if (person.getID().equals(newPerson.getID())) {
                persons.set(i, newPerson);
                return;
            }
        }

        System.out.println("Update Person Locally ERROR: Could not find Person to update locally");
    }
}
