package me.kuhlti.address.server;

import me.kuhlti.address.server.model.SPerson;

import javax.jws.*;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.UUID;

@WebService(name = "SPersonAdminWS")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class Server {

    // ====== Parameters ======

    private static ArrayList<SPerson> persons = new ArrayList<SPerson>();


    // ====== Initialization ======

    public Server() {
        generateBoilerplateData();
    }


    // ====== WebMethods ======

    @WebMethod public SPerson[] getAll() {
        System.out.println("Send All Persons: " + persons.size());

        SPerson[] array = new SPerson[persons.size()];
        return persons.toArray(array);
    }

    @WebMethod public void add(SPerson sPerson) {
        if (getIndexFor(sPerson.getID()) == null) {
            persons.add(sPerson);
            System.out.println("Added: " + sPerson.fullName());
        } else {
            System.out.println("Error Adding: Already found a person with the same ID " + sPerson.getID());
        }
    }

    @WebMethod public void update(SPerson sPerson) {
        Integer index = getIndexFor(sPerson.getID());

        if (index != null) {
            persons.set(index, sPerson);
            System.out.println("Updated: " + sPerson.fullName());
        } else {
            System.out.println("Error Updating: Could not find Person with ID " + sPerson.getID());
        }
    }

    @WebMethod public void delete(SPerson sPerson) {
        Integer index = getIndexFor(sPerson.getID());

        if (index != null) {
            persons.remove((int) index);
            System.out.println("Removed: " + sPerson.fullName());
        } else {
            System.out.println("Error Deleting: Could not find Person with ID " + sPerson.getID());
        }

    }

    // ====== Functions ======

    private void generateBoilerplateData() {
        add(new SPerson("Chandler", "Bing"));
        add(new SPerson("Rachel", "Green"));
        add(new SPerson("Joey", "Tribbiani"));
        add(new SPerson("Ross", "Geller"));
    }

    /**
     * Gets the Index for the given ID
     * @param id The ID of the person you want the index of.
     * @return the index
     */
    private Integer getIndexFor(String id) {
        for (int i = 0; i < persons.size(); i++) {
            SPerson sPerson = persons.get(i);
            if (sPerson.getID().equals(id)) {
                return i;
            }
        }

        return null;
    }

}
