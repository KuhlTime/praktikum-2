package me.kuhlti.address.server.model;

import java.time.LocalDate;
import java.util.UUID;

public class SPerson {

	// ====== Properties ======

	private String id 			= "";
	private String firstName 	= "";
	private String lastName 	= "";
	private String city 		= "";
	private String street 		= "";
	private Integer postalCode 	= 0;
	private String birthday 	= LocalDate.now().toString();


	// ====== Initialization ======

	// Important otherwise does not work
	public SPerson() {}

	public SPerson(String firstName, String lastName, String city, String street, Integer postalCode, String birthday) {
		this.id 		= generateID();
		this.firstName 	= firstName;
		this.lastName 	= lastName;
		this.city 		= city;
		this.street 	= street;
		this.postalCode = postalCode;
		this.birthday 	= birthday;
	}

	public SPerson(String firstName, String lastName) {
		this.id 		= generateID();
		this.firstName 	= firstName;
		this.lastName 	= lastName;
	}


	// ====== Functions ======

	public String fullName() { return getFirstName() + " " + getLastName(); }

	public String generateID() {
		return UUID.randomUUID().toString();
	}

	// ====== Getters ======

	public String getID() { return id; }

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public String getBirthday() {
		return birthday;
	}


	// ====== Setters ======

	public void setID(String id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName=lastName;
	}

	public void setCity(String city) {
		this.city =city;
	}

	public void setStreet(String street) {
		this.street =street;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
}
